#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "./structs.h"
#include "./comparator.h"
#define MAX_JOBS 100 //arbitrary number

struct Job jobs[MAX_JOBS];
struct Job fifoJobs[MAX_JOBS];
struct Job sjfJobs[MAX_JOBS];
struct Job stcfJobs[MAX_JOBS];
struct Job rr1Jobs[MAX_JOBS];
struct Job rr2Jobs[MAX_JOBS];

struct Stats fifoStats[MAX_JOBS];
struct Stats sjfStats[MAX_JOBS];
struct Stats stcfStats[MAX_JOBS];
struct Stats rr1Stats[MAX_JOBS];
struct Stats rr2Stats[MAX_JOBS];

short jobCount = 0;
short timeSlice1 = 2;
short timeSlice2 = 3;

void getJobsAtArrivalTime(struct Job a[], int num_elements, int value, struct Job returnJobs[]);
struct Job copyJobStruct(struct Job a);
void spliceArray(int position, struct Job a[]);
struct Stats copyStatsStruct(struct Stats s);
int calculateTurnaroundTime(struct Stats s);
int calculateResponseTime(struct Stats s);
void setEndTime(struct Stats a[], char *pid, int num_elements, int endTime);
void setStartTime(struct Stats a[], char *pid, int num_elements, int startTime);
double calculateAverageTurnaroundTime(struct Stats a[], int num_elements);
double calculateAverageResponseTime(struct Stats a[], int num_elements);
void moveElemToEnd(struct Job a[], int count);

int main(void)
{
    int totalRuntime = 0;
    char *line = NULL;
    size_t size;
    size_t read;
    while ((read = getline(&line, &size, stdin) > 0) && jobCount < MAX_JOBS)
    {
        //job_id arrival_time run_time
        printf("? %s", line);
        char *token = strtok(line, " ");
        char *jobId = token;
        int arrivalTime = atoi(strtok(NULL, " "));
        int runTime = atoi(strtok(NULL, " "));
        totalRuntime += runTime;
        jobs[jobCount].arrivalTime = arrivalTime;
        jobs[jobCount].runTime = runTime;
        strcpy(jobs[jobCount].pid, jobId);

        struct Stats stat;
        strcpy(stat.pid, jobId);
        stat.arrivalTime = arrivalTime;
        stat.startTime = -1;
        stat.endTime = 0;
        fifoStats[jobCount].arrivalTime = arrivalTime;
        fifoStats[jobCount].startTime = -1;
        fifoStats[jobCount].endTime = 0;
        strcpy(fifoStats[jobCount].pid, jobId);

        sjfStats[jobCount].arrivalTime = arrivalTime;
        sjfStats[jobCount].startTime = -1;
        sjfStats[jobCount].endTime = 0;
        strcpy(sjfStats[jobCount].pid, jobId);

        stcfStats[jobCount].arrivalTime = arrivalTime;
        stcfStats[jobCount].startTime = -1;
        stcfStats[jobCount].endTime = 0;
        strcpy(stcfStats[jobCount].pid, jobId);

        rr1Stats[jobCount].arrivalTime = arrivalTime;
        rr1Stats[jobCount].startTime = -1;
        rr1Stats[jobCount].endTime = 0;
        strcpy(rr1Stats[jobCount].pid, jobId);

        rr2Stats[jobCount].arrivalTime = arrivalTime;
        rr2Stats[jobCount].startTime = -1;
        rr2Stats[jobCount].endTime = 0;
        strcpy(rr2Stats[jobCount].pid, jobId);

        jobCount++;
    }
    // copy array into individual arrays for diff scheduling algos
    memcpy(&fifoJobs, &jobs, sizeof(jobs));
    memcpy(&stcfJobs, &jobs, sizeof(jobs));
    memcpy(&sjfJobs, &jobs, sizeof(jobs));
    memcpy(&rr1Jobs, &jobs, sizeof(jobs));
    memcpy(&rr2Jobs, &jobs, sizeof(jobs));
    // qsort((void *)sjfJobs, jobCount, sizeof(sjfJobs[0]), rt_comparator);
    printf("\nT\tFIFO\tSJF\tSTCF\tRR1\tRR2\n");
    int timeUnit = 0, fifoCount = 0, stcfCount = 0, sjfCount = 0, rr1Count = 0, rr2Count = 0;
    int rr1CpuCount = 0, rr2CpuCount = 0;
    while (1)
    {

        struct Job jobArrive[jobCount];
        memset(jobArrive, 0, sizeof(jobArrive));
        getJobsAtArrivalTime(jobs, jobCount, timeUnit, jobArrive);
        for (int i = 0; i < jobCount; i++)
        {
            if (jobArrive[i].runTime)
            {
                printf("* ARRIVED: %s\n", jobArrive[i].pid);
                fifoJobs[fifoCount] = copyJobStruct(jobArrive[i]);
                stcfJobs[stcfCount] = copyJobStruct(jobArrive[i]);
                sjfJobs[sjfCount] = copyJobStruct(jobArrive[i]);
                rr1Jobs[rr1Count] = copyJobStruct(jobArrive[i]);
                rr2Jobs[rr2Count] = copyJobStruct(jobArrive[i]);

                rr2Count++;
                rr1Count++;
                fifoCount++;
                stcfCount++;
                sjfCount++;
            }
        }
        memset(jobArrive, 0, sizeof(jobArrive)); //clear the array for next batch

        printf("%d\t", timeUnit);
        /*Fifo*/
        if (fifoJobs[0].arrivalTime <= timeUnit && fifoJobs[0].runTime > 0 && fifoCount > 0)
        {
            setStartTime(fifoStats, fifoJobs[0].pid, jobCount, timeUnit);
            printf("%s\t", fifoJobs[0].pid);
            fifoJobs[0].runTime--;
        }
        else
        {
            printf("\t");
        }
        /*fifo end*/

        /*shortest job first*/
        if (sjfJobs[0].arrivalTime <= timeUnit && sjfJobs[0].runTime > 0 && sjfCount > 0)
        {
            setStartTime(sjfStats, sjfJobs[0].pid, jobCount, timeUnit);
            printf("%s\t", sjfJobs[0].pid);
            sjfJobs[0].runTime--;
        }
        else
        {
            printf("\t");
        }
        /*shortest job first end*/

        /*shortest time to completion first*/
        qsort((void *)stcfJobs, stcfCount, sizeof(stcfJobs[0]), rt_comparator);
        if (stcfJobs[0].arrivalTime <= timeUnit && stcfJobs[0].runTime > 0 && stcfCount > 0)
        {
            setStartTime(stcfStats, stcfJobs[0].pid, jobCount, timeUnit);
            printf("%s\t", stcfJobs[0].pid);
            stcfJobs[0].runTime--;
        }
        else
        {
            printf("\t");
        }
        /*shortest time to completion first end*/
        /* RR1 start*/
        if (rr1Jobs[0].runTime > 0 && rr1Jobs[0].arrivalTime <= timeUnit && rr1Count > 0)
        {
            if (rr1CpuCount == timeSlice1)
            {
                rr1CpuCount = 0;
                moveElemToEnd(rr1Jobs, rr1Count);
            }
            setStartTime(rr1Stats, rr1Jobs[0].pid, jobCount, timeUnit);
            printf("%s\t", rr1Jobs[0].pid);
            rr1Jobs[0].runTime--;
            rr1CpuCount++;
        }
        else
        {
            printf("\t");
        }
        /* RR1 end*/
        /* RR2 start*/
        if (rr2Jobs[0].runTime > 0 && rr2Jobs[0].arrivalTime <= timeUnit && rr2Count > 0)
        {
            if (rr2CpuCount == timeSlice2)
            {
                rr2CpuCount = 0;
                moveElemToEnd(rr2Jobs, rr2Count);
            }
            setStartTime(rr2Stats, rr2Jobs[0].pid, jobCount, timeUnit);
            printf("%s\t", rr2Jobs[0].pid);
            rr2Jobs[0].runTime--;
            rr2CpuCount++;
        }
        else
        {
            printf("\t");
        }
        /* RR2 end*/

        if (fifoJobs[0].runTime == 0 && fifoCount > 0)
        {
            printf("\n* Complete: %s", (fifoJobs[0]).pid);
            // printf("\n? FIFO Completed: %s", (fifoJobs[0]).pid);
            setEndTime(fifoStats, fifoJobs[0].pid, jobCount, timeUnit);
            spliceArray(0, fifoJobs);
            fifoCount--;
        }
        if (sjfJobs[0].runTime == 0 && sjfCount > 0)
        {
            printf("\n* Complete: %s", (sjfJobs[0]).pid);
            // printf("\n? SJF Completed: %s", (sjfJobs[0]).pid);
            setEndTime(sjfStats, sjfJobs[0].pid, jobCount, timeUnit);
            spliceArray(0, sjfJobs);
            sjfCount--;
            qsort((void *)sjfJobs, sjfCount, sizeof(sjfJobs[0]), rt_comparator);
        }
        if (stcfJobs[0].runTime == 0 && stcfCount > 0)
        {
            printf("\n* Complete: %s", (stcfJobs[0]).pid);
            // printf("\n? STCF Completed: %s", (stcfJobs[0]).pid);
            setEndTime(stcfStats, stcfJobs[0].pid, jobCount, timeUnit);
            spliceArray(0, stcfJobs);
            stcfCount--;
        }
        if (rr1Jobs[0].runTime == 0 && rr1Count > 0)
        {
            printf("\n* Complete: %s", (rr1Jobs[0]).pid);
            setEndTime(rr1Stats, rr1Jobs[0].pid, jobCount, timeUnit);
            spliceArray(0, rr1Jobs);
            rr1Count--;
            rr1CpuCount = 0;
        }

        if (rr2Jobs[0].runTime == 0 && rr2Count > 0)
        {
            printf("\n* Complete: %s", (rr2Jobs[0]).pid);
            setEndTime(rr2Stats, rr2Jobs[0].pid, jobCount, timeUnit);
            spliceArray(0, rr2Jobs);
            rr2Count--;
            rr2CpuCount = 0;
        }

        printf("\n");
        timeUnit++;
        if (timeUnit > totalRuntime + 5)
            break;
    }
    printf("= SIMULATION COMPLETE\n");
    /*************Stats*******************/

    printf("#\tJOB\tFIFO\tSJF\tSTCF\tRR1\tRR2\n");
    for (int i = 0; i < jobCount; i++)
    {
        printf("T\t%s\t%d\t%d\t%d\t%d\t%d\n", stcfStats[i].pid,
               calculateTurnaroundTime(fifoStats[i]), calculateTurnaroundTime(sjfStats[i]),
               calculateTurnaroundTime(stcfStats[i]), calculateTurnaroundTime(rr1Stats[i]),
               calculateTurnaroundTime(rr2Stats[i]));
    }
    for (int i = 0; i < jobCount; i++)
    {
        printf("R\t%s\t%d\t%d\t%d\t%d\t%d\n", stcfStats[i].pid,
               calculateResponseTime(fifoStats[i]), calculateResponseTime(sjfStats[i]),
               calculateResponseTime(stcfStats[i]), calculateResponseTime(rr1Stats[i]),
               calculateResponseTime(rr2Stats[i]));
    }
    printf("= INDIVIDUAL STATS COMPLETE\n");

    printf("#\tSCHEDULER\tAVG_TURNAROUND\tAVG_RESPONSE\n");
    {
        printf("@\tFIFO\t\t%f\t\t%f\n", calculateAverageTurnaroundTime(fifoStats, jobCount), calculateAverageResponseTime(fifoStats, jobCount));
        printf("@\tSJF\t\t%f\t\t%f\n", calculateAverageTurnaroundTime(sjfStats, jobCount), calculateAverageResponseTime(sjfStats, jobCount));
        printf("@\tSTCF\t\t%f\t\t%f\n", calculateAverageTurnaroundTime(stcfStats, jobCount), calculateAverageResponseTime(stcfStats, jobCount));
        printf("@\tRR1\t\t%f\t\t%f\n", calculateAverageTurnaroundTime(rr1Stats, jobCount), calculateAverageResponseTime(rr1Stats, jobCount));
        printf("@\tRR2\t\t%f\t\t%f\n", calculateAverageTurnaroundTime(rr2Stats, jobCount), calculateAverageResponseTime(rr2Stats, jobCount));
    }
    printf("= AGGREGATE STATS COMPLETE\n");
    return 0;
}

void getJobsAtArrivalTime(struct Job a[], int num_elements, int value, struct Job returnJobs[])
{
    int tempArrayCount = 0;
    for (int i = 0; i < num_elements; i++)
    {
        if (a[i].arrivalTime == value)
        {
            returnJobs[tempArrayCount] = a[i];
            tempArrayCount++;
        }
    }
}

struct Job copyJobStruct(struct Job a)
{
    struct Job b;
    b.arrivalTime = a.arrivalTime;
    b.runTime = a.runTime;
    strcpy(b.pid, a.pid);
    return b;
}

void spliceArray(int position, struct Job a[])
{
    for (int c = position - 1; c < jobCount; c++)
        a[c] = a[c + 1];
}

void moveElemToEnd(struct Job a[], int count)
{
    a[count] = a[0];
    spliceArray(0, a);
}

struct Stats copyStatsStruct(struct Stats a)
{
    struct Stats b;
    b.startTime = a.startTime;
    b.endTime = b.startTime;
    strcpy(b.pid, a.pid);
    return b;
}

int calculateTurnaroundTime(struct Stats s)
{
    return s.endTime - s.arrivalTime;
}

int calculateResponseTime(struct Stats s)
{
    return s.startTime - s.arrivalTime;
}

void setEndTime(struct Stats a[], char *pid, int num_elements, int endTime)
{
    for (int i = 0; i < num_elements; i++)
    {
        if (strcmp(a[i].pid, pid) == 0)
        {
            a[i].endTime = endTime;
            return;
        }
    }
}

void setStartTime(struct Stats a[], char *pid, int num_elements, int startTime)
{
    for (int i = 0; i < num_elements; i++)
    {
        if (strcmp(a[i].pid, pid) == 0)
        {
            if (a[i].startTime == -1)
            {
                a[i].startTime = startTime;
            }
            return;
        }
    }
}

double calculateAverageTurnaroundTime(struct Stats a[], int num_elements)
{
    int total = 0;
    for (int i = 0; i < num_elements; i++)
    {
        total += calculateTurnaroundTime(a[i]);
    }
    return total / num_elements;
}

double calculateAverageResponseTime(struct Stats a[], int num_elements)
{
    int total = 0;
    for (int i = 0; i < num_elements; i++)
    {
        total += calculateResponseTime(a[i]);
    }
    return total / num_elements;
}