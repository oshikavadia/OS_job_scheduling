#ifndef COMPARATOR_H
#define COMPARATOR_H

int rt_comparator(const void *v1, const void *v2); // runtime comaparator
int at_comparator(const void *v1, const void *v2); // arrival time comaparator
int stcf_comparator(const void *v1, const void *v2); // shortest time to completion first comaparator

#endif
